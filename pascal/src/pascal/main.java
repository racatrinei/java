package pascal;
import java.util.*;

public class main {

	public static void main (String[] args) {
	  
		Scanner in = new Scanner(System.in);
		System.out.println("Introduceti nr de randuri: ");
       
		int n = in.nextInt();
		int [][]a = new int[20][20];
		int i,j;
		
		for ( i = 0; i<n; i++){ 
			a[i][0] = 1; // primul termen din rand este mereu 1
			a[i][1] = i; //al doilea termen din rand este mereu egal cu numarul randului
			a[i][i] = 1; // ultimul termen din rand este mereu 1
		}
		
		for(i = 3; i<n;i++) 
			for(j=2;j<n;j++)
				a[i][j] = a[i-1][j-1] + a[i-1][j]; //aflare termeni din mijloc 
		
		for(i = 0; i<n;i++){
			for(j=0;j<n;j++)
				if(a[i][j] !=0)
					System.out.print(" " + a[i][j]); //afisare triungghi
	
			System.out.println();
		}
		in.close();
	}

}
