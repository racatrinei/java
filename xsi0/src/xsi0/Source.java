package xsi0;
import java.util.*;

public class Source extends initGame {
	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
		_constructor();
		
		int win;
		int jocx = 0;
		int jocy = 0;
		boolean game = checkGame(a);
		int opt = 1;
		
		while(opt == 1){
			
			if(game){
				gameX();
				game = checkGame(a);
				if(game == false){ // daca nu mai sunt spatii goale si nu a iesit din joc atunci este remiza
					System.out.println("There is no winners");
					opt = 0;
					break;
				}
				if(jocx < 6){ // un jucator nu poate avea mai mult de 6 mutari
					
					win = checkWinner(a); //verificare daca exista un winner
					
					if(win == 1){  // daca exista un winner se termina jocul
						jocx = 6; 
						break;
					}
				}
				
				gameY();
				game = checkGame(a);
				if(game == false){ // daca nu mai sunt spatii goale si nu a iesit din joc atunci este remiza
					System.out.println("There is no winners");
					opt = 0;
					break;
				}
				if(jocy < 6){ // un jucator nu poate avea mai mult de 6 mutari
					
					win = checkWinner(a);
					
					if(win == 1){
						jocx = 6;
						break;
					}
				}
				game = checkGame(a); //verificare spatii goale
				jocx++; 
				jocy++;
			}
			
		}
		in.close();	
	}
}
