package xsi0;
import java.util.*;

public class initGame {
	public static char [][]a = new char [3][3]; 
	public static Scanner in = new Scanner(System.in);
	
	public static void _constructor(){  // initializare matrice
		
		for(int i = 0; i < 3;i++)
			for (int j = 0; j<3;j++)
				a[i][j] = ' ';
	}
	
	public static void afisare(char a[][]){   // afisare matrice 
		
		for(int i = 0; i < 3;i++){
			for (int j = 0; j < 3;j++)
				System.out.print(a[i][j] + "|");
			System.out.println();
		}
	}
	
	public static void gameX(){
	
		System.out.print("Player X insert your move's coordonates (row[1-3] column[1-3]): ");
		int row,col;
		
		row = in.nextInt()-1;
		col = in.nextInt()-1;
		
		if (a[row][col] == ' ') // verificare daca locul indicat este liber 
			a[row][col] = 'x';
		else{
			System.out.println("This move at (" + (row + 1) + "," + (col + 1) + ") is not valid. Please try again...");
			row = in.nextInt()-1;
			col = in.nextInt()-1;
			gameX();
		}
		afisare(a); // afisare joc dupa mutare
	}
	public static void gameY(){
		int row,col;
		System.out.print("Player 0 insert your move's coordonates (row[1-3] column[1-3]): ");
		row = in.nextInt()-1;
		col = in.nextInt()-1;
		if (a[row][col] == ' ') // verificare loc
			a[row][col] = '0';
		else{
			System.out.println("This move at (" + (row + 1) + "," + (col + 1) + ") is not valid. Please try again...");
			gameY();
		}
		afisare(a); // afisare joc dupa mutare

	}
	public static boolean checkGame(char a[][]){  // verificare daca mai sunt locuri pentru mutari
		boolean ok = false;
		for(int i = 0; i < 3;i++)
			for (int j = 0; j < 3;j++)
				if(a[i][j] == ' ')
					ok = true;
		return ok;
		
	}
	public static int checkWinner(char a[][]){
		
		if (a[0][0] == a[1][1] && a[0][0] == a[2][2] && a[0][0] != ' '){ // daca avem aceleasi valori pe diagonala
			System.out.println("The winner is " + a[0][0]);  
			return 1;
			}
		else
			if (a[0][2] == a[1][1] && a[0][2] == a[2][0]  && a[1][1] != ' '){ // daca avem aceleasi valori pe diagonala
				System.out.println("The winner is " + a[1][1]);  
				return 1;
				}
		
		int i = 0;
		
		if( a[i][i] != ' '){
			if ( (a[i][i] == a[i][i+1] && a[i][i] == a[i][i+2]) || ( a[i][i] == a[i+1][i] && a[i][i] == a[i+2][i])) // daca avem aceleasi valori pe linie si coloana
			{
				System.out.println("The winner is " + a[i][i]);
				return 1;
			}
		}
		return 0;
	}
	
}